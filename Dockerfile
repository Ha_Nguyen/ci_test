FROM ubuntu

LABEL NAME="sample-ci-cpp" \
      VERSION="latest" \
      DESC="Ubuntu 16.04 with added packages." \
      PACKAGES="cmake,g++,git,curl,libmysqlclient-dev,shellcheck,cpplint"

RUN apt-get update --yes && \
    apt-get install --yes g++ && \
    apt-get install --yes cmake && \
    apt-get install --yes git && \
    apt-get install --yes curl && \
    apt-get install --yes libmysqlclient-dev && \
    apt-get install -y shellcheck 

